let userName = prompt("Введіть своє ім'я").trim();
while (userName === "" || userName === null) {
  userName = prompt("Введіть своє ім'я знову").trim();
}
let age = prompt("Введіть свій вік");
while (age === "" || age === 0 || isNaN(age)) {
  age = prompt("Введіть свій вік знову", age);
}
if (age < 18) {
  alert("You are not allowed to visit this website");
} else if (age >= 18 && age <= 22) {
  let answer = confirm("Are you sure you want to continue?");
  answer
    ? alert(`Welcome, ${userName}`)
    : alert("You are not allowed to visit this website");
} else {
  alert(`Welcome, ${userName}`);
}
